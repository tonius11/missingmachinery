package tonius.missingmachinery.proxy;

import cpw.mods.fml.common.SidedProxy;

public class CommonProxy {

	@SidedProxy(clientSide = "tonius.missingmachinery.proxy.ClientProxy", serverSide = "tonius.missingmachinery.proxy.CommonProxy")
	public static CommonProxy proxy;
	
}
