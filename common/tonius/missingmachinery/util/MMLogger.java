package tonius.missingmachinery.util;

import java.util.logging.Logger;

public class MMLogger {

	private static Logger mmLogger;

	public static void initLogger(Logger logger) {
		mmLogger = logger;
	}
	
	public static void info(String string) {
		mmLogger.info(string);
	}
	
}
