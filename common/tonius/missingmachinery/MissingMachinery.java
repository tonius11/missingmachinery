package tonius.missingmachinery;

import java.io.File;

import net.minecraftforge.common.MinecraftForge;
import tonius.missingmachinery.config.MMConfig;
import tonius.missingmachinery.event.MMEventHandlers;
import tonius.missingmachinery.lang.MMLocalization;
import tonius.missingmachinery.util.MMLogger;
import codechicken.lib.lang.LangUtil;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = "MissingMachinery", name = "Missing Machinery", version = "@VERSION@", dependencies = "required-after:CodeChickenCore")
@NetworkMod(serverSideRequired = true, clientSideRequired = true)
public class MissingMachinery {
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent evt) {
		MMLogger.initLogger(evt.getModLog());
		MMLogger.info("Starting Missing Machinery version @VERSION@");		
		
		MMConfig.processConfig(new File(evt.getModConfigurationDirectory(), "MissingMachinery.cfg"));
	}
	
	@EventHandler
	public void init(FMLInitializationEvent evt) {
		MinecraftForge.EVENT_BUS.register(new MMEventHandlers());
		
		MMLocalization.initLang(LangUtil.loadLangDir("missingmachinery"));
	}
	
}