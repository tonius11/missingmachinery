package tonius.missingmachinery.item;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import tonius.missingmachinery.lang.MMLocalization;
import tonius.missingmachinery.lang.UnlocalizedNames;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemDrillTier3 extends ItemDrillBase {
	
	public final static double SPEED_SLOW = 7;
	public final static double SPEED_FAST = 15;
	
	public ItemDrillTier3(int id, EnumToolMaterial drillEnum) {
		super(id, drillEnum, UnlocalizedNames.NAME_DRILL_TIER_3, true);
		this.efficiencyOnProperMaterial = (float)SPEED_SLOW;
	}
	
    @Override
    public float getStrVsBlock(ItemStack stack, Block block, int meta) {
    	double speed = getSpeed(stack);
    	if (block == Block.oreRedstone || block == Block.oreRedstoneGlowing || block == Block.obsidian)
    		return (float)speed;
        if (ForgeHooks.isToolEffective(new ItemStack(Item.pickaxeIron), block, meta))
            return (float)speed;
        if (ForgeHooks.isToolEffective(new ItemStack(Item.shovelIron), block, meta))
            return (float)speed;
        return 1.0F;
    }
    
    @Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
		if(player.isSneaking())
		{
			double speed = getSpeed(itemstack);
			if(speed == SPEED_FAST) {
				itemstack = setSpeed(itemstack, SPEED_SLOW);
			} else {
				itemstack = setSpeed(itemstack, SPEED_FAST);
			}
			
			if(world.isRemote)
			{
				ChatMessageComponent msg = new ChatMessageComponent();
				msg.addKey("chat.missingmachinery.info.drill.currentmode");
				msg.addText(" ");
				
				if(speed == SPEED_FAST) {
					msg.addText(EnumChatFormatting.RED.toString());
					msg.addKey("chat.missingmachinery.info.drill.modes.precision");
				} else {
					msg.addText(EnumChatFormatting.GREEN.toString());
					msg.addKey("chat.missingmachinery.info.drill.modes.speed");
				}
				
				player.sendChatToPlayer(msg);
			}
			
			return itemstack;
		}
		
		return itemstack;
	}
    
    public ItemStack setSpeed(ItemStack itemstack, double speed) {
    	if(itemstack.stackTagCompound == null) {
			itemstack.setTagCompound(new NBTTagCompound());
		}
		
		itemstack.stackTagCompound.setDouble("Speed", speed);
		
		return itemstack;
    }
    
    public double getSpeed(ItemStack itemstack) {
    	if(itemstack.stackTagCompound == null) {
			return SPEED_SLOW;
		}
		
		return itemstack.stackTagCompound.getDouble("Speed");
    }
    
    @Override
    public void addInformation(ItemStack itemstack, EntityPlayer player, List infoList, boolean advanced) {
    	String temp = MMLocalization.translate("item.missingmachinery.drill.info.mode") + " ";
    	
    	if(getSpeed(itemstack) == SPEED_FAST) {
    		temp += EnumChatFormatting.GREEN + MMLocalization.translate("chat.missingmachinery.info.drill.modes.speed");
    	} else {
    		temp += EnumChatFormatting.RED + MMLocalization.translate("chat.missingmachinery.info.drill.modes.precision");
    	}
    	
    	infoList.add(temp);
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public EnumRarity getRarity(ItemStack itemstack)
    {
        return EnumRarity.uncommon;
    }
}
