package tonius.missingmachinery.item;

import net.minecraft.item.EnumToolMaterial;
import tonius.missingmachinery.lang.UnlocalizedNames;

public class ItemDrillTier1 extends ItemDrillBase {
	
	public ItemDrillTier1(int id, EnumToolMaterial drillEnum) {
		super(id, drillEnum, UnlocalizedNames.NAME_DRILL_TIER_1, false);
	}
	
}
