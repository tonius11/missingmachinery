package tonius.missingmachinery.item;

import net.minecraft.item.EnumToolMaterial;
import tonius.missingmachinery.lang.UnlocalizedNames;

public class ItemDrillTier2 extends ItemDrillBase {
	
	public ItemDrillTier2(int id, EnumToolMaterial drillEnum) {
		super(id, drillEnum, UnlocalizedNames.NAME_DRILL_TIER_2, false);
	}
	
}
