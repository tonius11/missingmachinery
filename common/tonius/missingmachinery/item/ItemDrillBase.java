package tonius.missingmachinery.item;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraftforge.common.ForgeHooks;
import tonius.missingmachinery.lang.MMLocalization;
import tonius.missingmachinery.lang.UnlocalizedNames;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemDrillBase extends ItemTool {
	
	private boolean canMineObby;
	private String unlocalizedName;
	
	public ItemDrillBase(int id, EnumToolMaterial drillEnum, String unlocalizedName, boolean canMineObby) {
		super(id, 0.0F, drillEnum, new Block[256]);
		setHasSubtypes(true);
		setUnlocalizedName(String.format("%s.%s.name", UnlocalizedNames.MODKEY, unlocalizedName));
		setCreativeTab(CreativeTabs.tabTools);
		this.canMineObby = canMineObby;
		this.unlocalizedName = unlocalizedName;
	}
	
	@Override
	public String getItemDisplayName(ItemStack itemstack) {
		return MMLocalization.translate(getUnlocalizedName());
	}
	
	@Override
    public float getStrVsBlock(ItemStack stack, Block block, int meta) {
		if (block == Block.obsidian && canMineObby)
			return efficiencyOnProperMaterial;
    	if (block == Block.oreRedstone || block == Block.oreRedstoneGlowing)
    		return efficiencyOnProperMaterial;
        if (ForgeHooks.isToolEffective(new ItemStack(Item.pickaxeIron), block, meta))
            return efficiencyOnProperMaterial;
        if (ForgeHooks.isToolEffective(new ItemStack(Item.shovelIron), block, meta))
            return efficiencyOnProperMaterial;
        return 1.0F;
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(UnlocalizedNames.MODKEY + ":" + unlocalizedName);
    }
	
}
