package tonius.missingmachinery.lang;

import codechicken.lib.lang.LangUtil;

public class MMLocalization {
	
	private static LangUtil mmLang;

	public static void initLang(LangUtil langutil) {
		mmLang = langutil;
	}
	
	public static String translate(String key) {
		return mmLang.translate(key);
	}
	
}
