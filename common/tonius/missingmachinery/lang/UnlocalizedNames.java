package tonius.missingmachinery.lang;

public class UnlocalizedNames {

	public static final String MODKEY = "missingmachinery";
	
	public static final String NAME_DRILL_TIER_1 = "drillTier1";
	public static final String NAME_DRILL_TIER_2 = "drillTier2";
	public static final String NAME_DRILL_TIER_3 = "drillTier3";
	public static final String NAME_DRILL_TIER_4 = "drillTier4";
	
}
