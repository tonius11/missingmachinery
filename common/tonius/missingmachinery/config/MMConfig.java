package tonius.missingmachinery.config;

import java.io.File;

import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Property;
import tonius.missingmachinery.item.ItemDrillTier1;
import tonius.missingmachinery.item.ItemDrillTier2;
import tonius.missingmachinery.item.ItemDrillTier3;
import tonius.missingmachinery.item.ItemDrillTier4;
import tonius.missingmachinery.lang.UnlocalizedNames;
import cpw.mods.fml.common.registry.GameRegistry;

public class MMConfig {

	private static Configuration mmConfig;
	
	public static void processConfig(File configFile) {
		mmConfig = new Configuration(configFile);
		
		try {
			
			mmConfig.load();
			
			Property drillTier1ID = mmConfig.getItem(UnlocalizedNames.NAME_DRILL_TIER_1, DefaultIDs.ID_DRILL_TIER_1);
			EnumToolMaterial mmDrillTier1Enum = EnumHelper.addToolMaterial("mmDrillTier1", 2, 200, 5.5F, 4.0F, 0);
			Item drillTier1 = new ItemDrillTier1(drillTier1ID.getInt() - 256, mmDrillTier1Enum);
			MinecraftForge.setToolClass(drillTier1, "pickaxe", mmDrillTier1Enum.getHarvestLevel());
			GameRegistry.registerItem(drillTier1, UnlocalizedNames.NAME_DRILL_TIER_1);
			
			Property drillTier2ID = mmConfig.getItem(UnlocalizedNames.NAME_DRILL_TIER_2, DefaultIDs.ID_DRILL_TIER_2);
			EnumToolMaterial mmDrillTier2Enum = EnumHelper.addToolMaterial("mmDrillTier2", 2, 200, 7.0F, 5.0F, 0);
			Item drillTier2 = new ItemDrillTier2(drillTier2ID.getInt() - 256, mmDrillTier2Enum);
			MinecraftForge.setToolClass(drillTier2, "pickaxe", mmDrillTier2Enum.getHarvestLevel());
			GameRegistry.registerItem(drillTier2, UnlocalizedNames.NAME_DRILL_TIER_2);
			
			Property drillTier3ID = mmConfig.getItem(UnlocalizedNames.NAME_DRILL_TIER_3, DefaultIDs.ID_DRILL_TIER_3);
			EnumToolMaterial mmDrillTier3Enum = EnumHelper.addToolMaterial("mmDrillTier3", 3, 200, 10.0F, 6.0F, 0);
			Item drillTier3 = new ItemDrillTier3(drillTier3ID.getInt() - 256, mmDrillTier3Enum);
			MinecraftForge.setToolClass(drillTier3, "pickaxe", mmDrillTier3Enum.getHarvestLevel());
			GameRegistry.registerItem(drillTier3, UnlocalizedNames.NAME_DRILL_TIER_3);
	
			Property drillTier4ID = mmConfig.getItem(UnlocalizedNames.NAME_DRILL_TIER_4, DefaultIDs.ID_DRILL_TIER_4);
			EnumToolMaterial mmDrillTier4Enum = EnumHelper.addToolMaterial("mmDrillTier4", 4, 200, 20.0F, 7.0F, 0);
			Item drillTier4 = new ItemDrillTier4(drillTier4ID.getInt() - 256, mmDrillTier4Enum);
			MinecraftForge.setToolClass(drillTier4, "pickaxe", mmDrillTier4Enum.getHarvestLevel());
			GameRegistry.registerItem(drillTier4, UnlocalizedNames.NAME_DRILL_TIER_4);
			
		} finally {
			
			if (mmConfig.hasChanged()) {
				mmConfig.save();
			}
			
		}
	}
	
}
