package tonius.missingmachinery.config;

public class DefaultIDs {

	public static final int ID_DRILL_TIER_1 = 8000;
	public static final int ID_DRILL_TIER_2 = 8001;
	public static final int ID_DRILL_TIER_3 = 8002;
	public static final int ID_DRILL_TIER_4 = 8003;
	
}
