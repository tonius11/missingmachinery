package tonius.missingmachinery.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import tonius.missingmachinery.item.ItemDrillTier4;

public class MMEventHandlers {

	@ForgeSubscribe
	public void harvestDrops(HarvestDropsEvent evt) {
		if(evt.harvester != null) {
			EntityPlayer player = evt.harvester;
			ItemStack heldItemStack = player.getHeldItem();
			if(heldItemStack != null) {
				Item heldItem = heldItemStack.getItem();
				if((heldItem != null) && heldItem instanceof ItemDrillTier4) {
					ItemDrillTier4 drill = (ItemDrillTier4)heldItem;
					if(drill.isDestructionMode(heldItemStack))
						evt.drops.clear();
				}
			}
		}
	}
	
}
